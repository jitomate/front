/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors')

module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,jsx,tsx}'],
  theme: {
    colors: {
      base: '#fcf4f1',
      top: '#e8c8be',
      accentColor: '#c2734e',
      title: '#8D8485',
      transparent: 'transparent',
      current: 'currentColor',
      slate: colors.slate,
      gray: colors.gray,
      zinc: colors.zinc,
      neutral: colors.neutral,
      stone: colors.stone,
      red: colors.red,
      orange: colors.orange,
      amber: colors.amber,
      yellow: colors.yellow,
      lime: colors.lime,
      green: colors.green,
      emerald: colors.emerald,
      teal: colors.teal,
      cyan: colors.cyan,
      sky: colors.sky,
      blue: colors.blue,
      indigo: colors.indigo,
      violet: colors.violet,
      purple: colors.purple,
      fuchsia: colors.fuchsia,
      pink: colors.pink,
      rose: colors.rose
    },
    fontFamily: {
      sans: ['Montserrat', 'ui-sans-serif'],
      serif: ['Lora', 'ui-serif'],
      mono: ['Cutive\\ Mono', 'ui-monospace'],
      title: ['Galada', 'ui-serif']
    },
    extend: {}
  },
  plugins: []
}
